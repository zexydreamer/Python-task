import argparse
from Finders.ac import aho_corasick as ac
from Finders.bf import bruteforce as bf
from Finders.bm import bm as bm
from Finders.kmp import knuth_morris_pratt as kmp
from Finders.rc import rc_hash as rc


def main():
    d = {'ac': ac, 'bf': bf, 'bm': bm, 'kmp': kmp, 'rc': rc}
    parser = argparse.ArgumentParser(description='Find substring in string. Only one algorithm per run!')
    parser.add_argument('-rc', help='Rabin-Carp algorithm', metavar='Rabin-Carp', type=str, nargs=2)
    parser.add_argument('-ac', help='Aho-Corasick algorithm', metavar='Aho-Corasick', type=str, nargs=2)
    parser.add_argument('-bf', help='Bruteforce algorithm', metavar='Bruteforce', type=str, nargs=2)
    parser.add_argument('-bm', help='Boyer–Moore algorithm', metavar='Boyer-Moore', type=str, nargs=2)
    parser.add_argument('-kmp', help='Knuth–Morris–Pratt algorithm', metavar='Knuth–Morris–Pratt', type=str, nargs=2)
    args = parser.parse_args().__dict__
    for k1 in args.keys():
        for k2 in d.keys():
            if args[k1] is not None and k1 == k2:
                print(d[k2](args[k1][0], args[k1][1]))


if __name__ == '__main__':
    main()
