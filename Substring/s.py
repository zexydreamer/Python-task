from Finders.ac import aho_corasick as ac
from Finders.bf import bruteforce as bf
from Finders.bm import bm as bm
from Finders.kmp import knuth_morris_pratt as kmp
from Finders.rc import rc_hash as rc


def stat(filename):
    algos = [ac, bf, bm, kmp, rc]
    with open('statistic.txt', 'w') as f:
        f.write('\n')
    word = 'indulgence contrast'
    s = 0
    for alg in algos:
        with open(filename) as t:
            text = t.read()
            for i in range(100):
                s += alg(text, word)
                if i == 99:
                    s /= 100
                    with open('statistic.txt', 'a') as statistic:
                        statistic.write(str(alg) + ' -- ')
                        statistic.write(str(s) + '\n')
                    s = 0
    return 1


def main():
    stat('best.txt')
    # stat('worst.txt')


if __name__ == '__main__':
    main()
