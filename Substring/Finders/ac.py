from memprof import memprof


@memprof
def aho_corasick(string, substring):
    if len(string) == 0 or len(substring) == 0:
        return -1
    alp = {}
    for i in range(len(substring)):
        alp[substring[i]] = 0
    dell = []
    for i in range(len(substring) + 1):
        dell.append({})
    for i in alp:
        dell[0][i] = 0
    for j in range(len(substring)):
        prev = dell[j][substring[j]]
        dell[j][substring[j]] = j + 1
        for i in alp:
            if j + 1 < len(substring):
                dell[j+1][i] = dell[prev][i]
    res = list()
    pos = 0
    for i in range(len(string)):
        if string[i] not in dell[pos]:
            dell[pos][string[i]] = 0
        pos = dell[pos][string[i]]
        if pos == len(substring):
            res.append(i - len(substring) + 1)
    return len(res)
