from timeit import default_timer
from memprof import memprof


@memprof
def bm(s, x):
    # start = default_timer()
    if len(s) == 0 or len(x) == 0:
        return -1
    d = table_shifts(x)
    result = 0
    lenX = i = j = k = len(x)
    while j > 0 and i <= len(s):
        if s[k-1] == x[j-1]:
            k -= 1
            j -= 1
            if j <= 0:
                result += 1
                if k + lenX + 1 <= len(s):
                    k += lenX + 1
                else:
                    break
                j = lenX
        else:
            i += d[ord(x[j-1])]
            j = lenX
            k = i
    # return default_timer() - start
    return result


def table_shifts(substring):
    d = dict()
    len_substring = len(substring)
    for i in range(len_substring):
        d[ord(substring[i])] = len_substring - i
    return d
