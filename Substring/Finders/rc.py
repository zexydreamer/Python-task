import math
from timeit import default_timer
from memprof import memprof


@memprof
def rc_hash(string, substring):
    # start = default_timer()
    if len(string) == 0 or len(substring) == 0:
        return -1
    result = 0
    flag = False
    for i in range(len(string)):
        for j in range(len(substring)):
            if getRCHash(string[i + j:i + j + len(substring)]) ==\
                 getRCHash(substring):
                for l in range(len(substring)):
                    if ord(string[i + l]) != ord(substring[l]):
                        break
                result += 1
            else:
                flag = True
                break
        if flag:
            i += len(substring)
            flag = False
    # return default_timer() - start
    return result


def getRCHash(string):
    sum = 0
    power = int(math.pow(2, len(string) - 1))
    for ch in string:
        sum += ord(ch) * power
        power >>= 1
    return sum
