from timeit import default_timer
from memprof import memprof


@memprof
def bruteforce(string, substring):
    # start = default_timer()
    if len(string) == 0 or len(substring) == 0:
        return -1
    result = 0
    flag = False
    if len(string) < len(substring):
        return 0
    for i in range(len(string)):
        for j in range(len(substring)):
            if i + j >= len(string):
                break
            if ord(string[i + j]) != ord(substring[j]):
                flag = True
                break
        if flag:
            i += len(substring)
            flag = False
        else:
            if i % len(substring) == 0:
                result += 1
    # duration = default_timer() - start
    return result
    # return duration
