from timeit import default_timer
from memprof import memprof


@memprof
def knuth_morris_pratt(string, substring):
    # start = default_timer()
    if len(string) == 0 or len(substring) == 0:
        return -1
    d = dict()
    d[0] = 0
    for i in range(1, len(substring)):
        j = d[i - 1]
        while j > 0 and substring[j] != substring[i]:
            j = d[j - 1]
        if substring[j] == substring[i]:
            j += 1
        d[i] = j
    i = 0
    j = 0
    m = len(string) - len(substring) + 1
    res = 0
    for u in range(m):
        while i < len(string) and j < len(substring):
            if substring[j] == string[i]:
                i += 1
                j += 1
            elif j == 0:
                i += 1
            else:
                j = d[j - 1]
        else:
            if j == len(substring):
                res += 1
                i += 1
                j = 0
    # return default_timer() - start
    return res
