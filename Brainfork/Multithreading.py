import VM
from threading import Thread
import Brainfork


class Multithreading:
    program = ''
    thread = Thread()
    thread_counter = 0

    @staticmethod
    def register_to(vm: VM, program: str):
        Multithreading.program = program
        Multithreading.multithreading_command(vm)

    @staticmethod
    def multithreading_command(vm: VM):
        vm.register_command('Y', Multithreading.multithreading)

    @staticmethod
    def multithreading(vm: VM):
        Multithreading.thread_counter += 1
        target = Brainfork.Brainfork.exec
        args = (Multithreading.program, vm.instructions_pointer + 1)
        Multithreading.thread = Thread(target=target, args=args)
        vm.memory[vm.memory_pointer] = 1
