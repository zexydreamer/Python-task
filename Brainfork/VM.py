from Multithreading import Multithreading


class VM:
    def __init__(self, program: str, memory_size: int, ip: int = 0):
        self.instructions = program
        self.memory = bytearray(memory_size)
        self.memory_pointer = 0
        self.instructions_pointer = ip
        self.commands = {}

    def register_command(self, ch: str, func):
        self.commands[ch] = func

    def execute(self):
        while self.instructions_pointer < len(self.instructions):
            thread = Multithreading.thread
            if Multithreading.thread_counter != 0:
                Multithreading.thread_counter = 0
                thread.start()
            instruction = self.instructions[self.instructions_pointer]
            if instruction in self.commands.keys():
                self.commands[instruction](self)
            self.instructions_pointer += 1
