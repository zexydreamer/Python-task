import VM


class BasicCommands:
    @staticmethod
    def register_to(vm: VM):
        BasicCommands.inc_dec_command(vm)
        BasicCommands.read_write_command(vm)
        BasicCommands.shift_command(vm)

    @staticmethod
    def inc_dec_command(vm: VM):
        vm.register_command('+', BasicCommands.inc_command)
        vm.register_command('-', BasicCommands.dec_command)

    @staticmethod
    def inc_command(vm: VM):
        if vm.memory[vm.memory_pointer] == 255:
            vm.memory[vm.memory_pointer] = 0
        else:
            vm.memory[vm.memory_pointer] += 1

    @staticmethod
    def dec_command(vm: VM):
        if vm.memory[vm.memory_pointer] == 0:
            vm.memory[vm.memory_pointer] = 255
        else:
            vm.memory[vm.memory_pointer] -= 1

    @staticmethod
    def read_write_command(vm: VM):
        vm.register_command('.', BasicCommands.write_command)
        vm.register_command(',', BasicCommands.read_command)

    @staticmethod
    def write_command(vm: VM):
        print(chr(vm.memory[vm.memory_pointer]), end='')

    @staticmethod
    def read_command(vm: VM):
        s = ord(input())
        vm.memory[vm.memory_pointer] = s

    @staticmethod
    def shift_command(vm: VM):
        vm.register_command('<', BasicCommands.left_shift_command)
        vm.register_command('>', BasicCommands.right_shift_command)

    @staticmethod
    def left_shift_command(vm: VM):
        if vm.memory_pointer == 0:
            vm.memory_pointer = len(vm.memory) - 1
        else:
            vm.memory_pointer -= 1

    @staticmethod
    def right_shift_command(vm: VM):
        if vm.memory_pointer == len(vm.memory) - 1:
            vm.memory_pointer = 0
        else:
            vm.memory_pointer += 1
