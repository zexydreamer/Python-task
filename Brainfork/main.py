from Brainfork import Brainfork
import sys
import argparse


def main():
    parser = argparse.ArgumentParser(
        description='Interpreter for the brainfork language')
    parser.add_argument('-f', metavar='File', type=str,
                        help='a file with brainfork program')
    if len(sys.argv) < 2:
        print(parser.print_help())
    else:
        args = parser.parse_args().__dict__
        Brainfork.exec(Brainfork.read(args['f']))


if __name__ == '__main__':
    main()
