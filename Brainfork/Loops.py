import VM
from Stack import Stack


class Loops:
    open_brackets = {}
    close_brackets = {}
    stack = Stack()

    @staticmethod
    def register_to(vm: VM):
        Loops.loops(vm)
        vm.register_command('[', Loops.open_bracket_command)
        vm.register_command(']', Loops.close_bracket_command)

    @staticmethod
    def loops(vm: VM):
        for i in range(len(vm.instructions)):
            bracket = vm.instructions[i]
            if bracket == '[':
                Loops.stack.push(i)
            elif bracket == ']':
                Loops.close_brackets[i] = Loops.stack.peek()
                Loops.open_brackets[Loops.stack.pop()] = i

    @staticmethod
    def open_bracket_command(vm: VM):
        if vm.memory[vm.memory_pointer] == 0:
            open_br = Loops.open_brackets[vm.instructions_pointer]
            vm.instructions_pointer = open_br

    @staticmethod
    def close_bracket_command(vm: VM):
        if vm.memory[vm.memory_pointer] != 0:
            close_br = Loops.close_brackets[vm.instructions_pointer]
            vm.instructions_pointer = close_br
