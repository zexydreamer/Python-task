import VM
from BasicCommands import BasicCommands
from Loops import Loops
import Multithreading


class Brainfork:
    @staticmethod
    def read(file: str) -> str:
        program = ''
        with open(file) as f:
            for line in f.readlines():
                program += line
        return program

    @staticmethod
    def exec(program: str, position: int = 0, memory_size: int = 1000):
        vm = VM.VM(program, memory_size, position)
        BasicCommands.register_to(vm)
        Loops.register_to(vm)
        Multithreading.Multithreading.register_to(vm, program)
        vm.execute()
